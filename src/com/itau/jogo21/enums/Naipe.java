package com.itau.jogo21.enums;

public enum Naipe {
  COPAS,
  PAUS,
  OUROS,
  ESPADAS;
}
